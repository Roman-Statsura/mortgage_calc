import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Program {
    final public static double INITIAL_PERCENT = 10;
    final public static int LIVING_WAGE = 15000;
    final public static int SUPPORT_THE_CHILD = 8000;
    final public static int AVERAGE_FLAT_RENT = 15000;
    final public static int MAX_AGE = 70;
    final public static double MAX_PAY = 0.6;
    static String fio;
    static String sex;
    static int age;
    static int militaryId = -1;
    static int creditTerm;
    static int salary;
    static boolean delays;
    static int children;
    static boolean flatOwnership;
    static int request;
    static int price;
    static int initialFee;

    public static void main(String[] args) {
        initialization();
        if (!checkForm(age, militaryId, delays, salary,
                children, flatOwnership,price,initialFee)){
            System.exit(300);
            System.out.printf("%s, к сожалению Вам отказанно в кредите",fio);
        }
        double increasePer = checkIncrPercent(age, request);
        int minIncome = calculateIncome(children, flatOwnership);
        int monthlyPay = paymentCalculation(creditTerm, request, increasePer);
        if (salary>(monthlyPay+minIncome) && monthlyPay<=(int)(salary*MAX_PAY) && (age+creditTerm/12)<=MAX_AGE){
            System.out.printf("Поздравляем! %s, Вам одобрена ипотека на сумму %d рублей, сроком на %d месяцев. " +
                    "Ежемесячный платеж составляет: %d рублей",fio,request,
                    creditTerm,monthlyPay);
        }else {
            ArrayList<Integer> res = newLoanOffer(creditTerm,age,salary,minIncome,increasePer);
            System.out.printf("%s,У нас для Вас новое предложение: Вам одобрена ипотека на сумму %d рублей, " +
                            "сроком на %d месяцев. Ежемесячный платеж составляет: %d рублей",fio,res.get(0),
                    res.get(1),res.get(2));
        }
    }

    public static int paymentCalculation(int creditTerm,int sum,double incrPercent){
        double pr = INITIAL_PERCENT + incrPercent;
        double monthPercent = pr/100/12;
        double monthlyPay = sum*(monthPercent+(monthPercent/(Math.pow((1+monthPercent),creditTerm)-1)));
        return (int) monthlyPay;
    }
    public static boolean checkForm(int age,int militaryId,boolean delays,int salary,
                                    int children,boolean flatOwnership,int price,int initialFee){
        return age >= 22 && age < MAX_AGE && militaryId != 0 && !delays
                && (salary>calculateIncome(children,flatOwnership) && (initialFee*100/price)>10);
    }
    public static int calculateIncome(int children,boolean flatOwnership){
        return LIVING_WAGE + SUPPORT_THE_CHILD*children + (flatOwnership? 0 : AVERAGE_FLAT_RENT);
    }
    public static double checkIncrPercent(int age,int request){
        double incPercent = 0;
        if (age<27) incPercent+=1.5;
        if (request<400000) incPercent+=0.5;
        return incPercent;
    }
    public static ArrayList<Integer> newLoanOffer(int creditTerm,int age,int salary,
                                                  int minIncome,double incrPercent ){
        int maxCreditTerm = (creditTerm/12+age)>MAX_AGE? (MAX_AGE-age)*12: 240;
        double pr = INITIAL_PERCENT + incrPercent;
        double monthPercent = pr/100/12;
        int freeMoney = salary - minIncome;
        int possiblePay = freeMoney>salary*MAX_PAY? (int)(salary*MAX_PAY) : freeMoney;

        int s = (int) (possiblePay/(monthPercent+(monthPercent/(Math.pow((1+monthPercent),maxCreditTerm)-1))));
        ArrayList<Integer> result = new ArrayList<>();
        result.add(s);
        result.add(maxCreditTerm);
        result.add(possiblePay);
        return result;
    }
    public static void initialization(){
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.println("Введите ФИО:");
            fio = bufferedReader.readLine();

            System.out.println("Введите пол:");
            sex = bufferedReader.readLine();

            System.out.println("Введите свой возраст:");
            age = Integer.parseInt(bufferedReader.readLine());
            if (age<=0) throw new ArithmeticException("Возраст не может быть отрицательным");

            if (sex.equals("мужской") && age<30){
                System.out.println("Есть ли у Вас военный билет? Введите 'да'/'нет'");
                String s = bufferedReader.readLine();
                if (s.equals("да"))
                    militaryId = 1;
                else militaryId = 0;
            }else militaryId = -1;

            System.out.println("Стоимость жилья:");
            price = Integer.parseInt(bufferedReader.readLine());
            if (price<=0) throw new ArithmeticException("Стоимость не может быть отрицательной");

            System.out.println("Введите первоначальный взнос (должен быть больше 10%)");
            initialFee = Integer.parseInt(bufferedReader.readLine());
            if (initialFee<0) throw new ArithmeticException("Первоначальный взнос не может быть отрицательным");

            if (initialFee>=price) throw new ArithmeticException("Первоначальный взнос не может быть больше стоимости жилья");
            request =  price - initialFee;

            System.out.println("Введите желаемый срок кредита (1-20 лет):");
            creditTerm = 12*Integer.parseInt(bufferedReader.readLine());
            if (creditTerm<=0) throw new ArithmeticException("Срок кредита должен быть положительным");
            if (creditTerm/12>20) creditTerm = 240;

            System.out.println("Введите свою з/п:");
            salary = Integer.parseInt(bufferedReader.readLine());
            if (salary<0) throw new ArithmeticException("З/п не может быть отрицательной");

            System.out.println("Были ли у вас просрочки по кредитам? Введите 'да'/'нет':");
            String wr = bufferedReader.readLine();
            delays = wr.trim().equalsIgnoreCase("да");

            System.out.println("Сколько у Вас несовершеннолетних детей?");
            children = Integer.parseInt(bufferedReader.readLine());

            System.out.println("Вы проживаете в своей собственной квартире? Введите 'да'/'нет':");
            String s = bufferedReader.readLine();
            flatOwnership = s.equals("да");
        } catch (Exception e) {
            System.out.println("Некорректный формат ввода");
        }
    }
}
